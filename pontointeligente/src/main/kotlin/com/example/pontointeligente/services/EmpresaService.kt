package com.example.pontointeligente.services

import com.example.pontointeligente.documents.Empresa

interface EmpresaService {

    fun findById(id: String): Empresa?

    fun findByCnpj(cnpj: String): Empresa?

    fun persistir(empresa: Empresa): Empresa
}