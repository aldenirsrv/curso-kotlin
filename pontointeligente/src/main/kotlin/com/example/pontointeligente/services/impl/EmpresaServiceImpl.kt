package com.example.pontointeligente.services.impl

import com.example.pontointeligente.documents.Empresa
import com.example.pontointeligente.repositories.EmpresaRepository
import com.example.pontointeligente.services.EmpresaService
import org.springframework.stereotype.Service

@Service
class EmpresaServiceImpl(val empresaRepository: EmpresaRepository) : EmpresaService {

    override fun findById(id: String): Empresa = this.empresaRepository.findByCnpj(id)

    override fun findByCnpj(cnpj: String): Empresa? = this.empresaRepository.findByCnpj(cnpj)

    override fun persistir(empresa: Empresa): Empresa = this.empresaRepository.save(empresa)
}