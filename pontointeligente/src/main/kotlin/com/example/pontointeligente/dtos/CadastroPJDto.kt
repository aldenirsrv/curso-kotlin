package com.example.pontointeligente.dtos

import org.hibernate.validator.constraints.Length
import org.hibernate.validator.constraints.br.CNPJ
import org.hibernate.validator.constraints.br.CPF
import javax.validation.constraints.Email
import javax.validation.constraints.NotEmpty

class CadastroPJDto (
    @get:NotEmpty(message = "Nome não pode ser vazio.")
    @get:Length(min = 3, max = 200, message = "O nome deve conter entre 3 e 200 caracteres.")
    val nome:String = "",

    @get:NotEmpty(message = "Email não pode ser vazio.")
    @get:Length(min = 3, max = 200, message = "O email deve conter entre 3 e 200 caracteres.")
    @get:Email(message = "Email inválido")
    val email:String = "",

    @get:NotEmpty(message = "Senha não pode ser vazia.")
    val senha:String = "",

    @get:NotEmpty(message = "CPF não pode ser vazio.")
    @get:CPF(message = "CPF não pode ser vazio")
    val cpf:String = "",

    @get:NotEmpty(message = "CPF não pode ser vazio.")
    @get:CNPJ(message = "CPF não pode ser vazio")
    val cnpj:String = "",

    @get:NotEmpty(message = "Email não pode ser vazio.")
    @get:Length(min = 3, max = 200, message = "O email deve conter entre 3 e 200 caracteres.")
    @get:Email(message = "Email inválido")
    val razaoSocial:String = "",

    id: String? = null
)