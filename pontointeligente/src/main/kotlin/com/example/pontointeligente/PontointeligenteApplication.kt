package com.example.pontointeligente

import com.example.pontointeligente.documents.Empresa
import com.example.pontointeligente.documents.Funcionario
import com.example.pontointeligente.enums.PerfilEnum
import com.example.pontointeligente.repositories.EmpresaRepository
import com.example.pontointeligente.repositories.FuncionarioRepository
import com.example.pontointeligente.utils.SenhaUtils
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class PontointeligenteApplication(val empresaRepository: EmpresaRepository, val funcionarioRepository: FuncionarioRepository) : CommandLineRunner {
	override fun run(vararg args: String?){
//		empresaRepository.deleteAll()
//		funcionarioRepository.deleteAll()
//
//		var empresa: Empresa = Empresa("Empresa LTDA", "35417492000108")
//		empresa = empresaRepository.save(empresa)
//
//		var admin: Funcionario = Funcionario("Isadora Giongo", "isa@gmail.com",
//				SenhaUtils().gerarBcrypt("123456"), "42357696001", PerfilEnum.ROLE_ADMIN, empresa.id!!)
//		admin = funcionarioRepository.save(admin)
//
//		var funcionario: Funcionario
//				= Funcionario("Veronica Torres", "veronica@gmail.com",
//				SenhaUtils().gerarBcrypt("654321"), "17347006023",
//				PerfilEnum.ROLE_USUARIO, empresa.id!!)
//		funcionario = funcionarioRepository.save(funcionario)
//
//		System.out.println("Empresa ID: " + empresa.id)
//		System.out.println("Admin ID: " + admin.id)
//		System.out.println("Funcionário ID: " + funcionario.id)
	}
}

fun main(args: Array<String>) {
	runApplication<PontointeligenteApplication>(*args)
}
