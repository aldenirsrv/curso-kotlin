package com.example.pontointeligente.controllers

import com.example.pontointeligente.documents.Funcionario
import com.example.pontointeligente.documents.Lancamento
import com.example.pontointeligente.dtos.LancamentoDto
import com.example.pontointeligente.response.Response
import com.example.pontointeligente.services.FuncionarioService
import com.example.pontointeligente.services.LancamentoService
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.validation.BindingResult
import org.springframework.validation.ObjectError
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("/api/lancamentos")
class LancamentoController(val lancamentoService: LancamentoService, val funcionarioService: FuncionarioService) {

    @Value("\${paginacacao.qtd_por_pagina}")
    val qtdPorPagina: Int = 15

    @PostMapping
    fun adicionar(@Valid @RequestBody lancamentoDto: LancamentoDto, result: BindingResult) : ResponseEntity<Response<Lancamento>> {
        val response: Response<LancamentoDto> = Response<LancamentoDto>()
    }

    private fun validarFuncionario(lancamentoDto: LancamentoDto, result: BindingResult){
        if(lancamentoDto.funcionarioId === null) {
            result.addError(ObjectError("funcionario", "Funcionário não informado"))
        }

        val funcionario: Funcionario? = funcionarioService.buscarPorId(lancamentoDto.funcionarioId)
        if()
    }

}