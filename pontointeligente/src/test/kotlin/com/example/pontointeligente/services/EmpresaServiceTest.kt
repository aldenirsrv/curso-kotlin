package com.example.pontointeligente.services

import com.example.pontointeligente.documents.Empresa
import com.example.pontointeligente.repositories.EmpresaRepository
import org.junit.jupiter.api.*
import org.mockito.BDDMockito
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.event.annotation.BeforeTestExecution
import org.springframework.test.context.junit4.SpringRunner
import java.lang.Exception
import kotlin.reflect.KClass

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest
class EmpresaServiceTest {


    @Autowired
    val empresaService: EmpresaService? = null

    @MockBean
    private val empresaRepository: EmpresaRepository? = null

    private val CNPJ = "51463645000100"

    @BeforeEach
    @Throws(Exception::class)
    fun setUp() {
        BDDMockito.given(empresaRepository?.findByCnpj(CNPJ)).willReturn(empresa())
//        BDDMockito.given(empresaRepository?.save(empresa())).willReturn(empresa())
        BDDMockito.given(empresaRepository?.save(Mockito.any(Empresa::class.java)))
                .willReturn(empresa())
    }

    @Test
    fun testBuscarEmpresaPorCnpj(){
        val empresa: Empresa? = empresaService?.findByCnpj(CNPJ)
        Assertions.assertNotNull(empresa)
    }

    @Test
    fun testPersistirEmpresa() {
        val empresa: Empresa? = empresaService?.persistir(empresa())
        Assertions.assertNotNull(empresa)
    }

    private fun empresa(): Empresa = Empresa(CNPJ, "Nova razão social")
}