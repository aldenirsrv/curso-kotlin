package com.example.pontointeligente.services

import com.example.pontointeligente.documents.Funcionario
import com.example.pontointeligente.documents.Lancamento
import com.example.pontointeligente.enums.PerfilEnum
import com.example.pontointeligente.enums.TipoEnum
import com.example.pontointeligente.repositories.LancamentoRepository
import com.example.pontointeligente.utils.SenhaUtils
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.mockito.BDDMockito
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import java.util.*
import kotlin.collections.ArrayList

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest
class LancamentoServiceTest {
    @MockBean
    private val lancamentRepository:LancamentoRepository? = null


    @Autowired
    private val lancamentoService: LancamentoService? = null

    private val id: String = "1"

    @BeforeEach
    @Throws(Exception::class)
    fun setup() {
        BDDMockito.given<Page<Lancamento>>(lancamentRepository?.findByFuncionarioId(id, PageRequest.of(1,10)))
                .willReturn(PageImpl(ArrayList<Lancamento>()))
        BDDMockito.given(lancamentRepository?.save(Mockito.any(Lancamento::class.java)))
                .willReturn(lancamento())
        // BDDMockito.given(lancamentRepository?.findOneLanc(id)).willReturn(lancamento())
    }

    @Test
    fun testBuscarLancamentoPorFuncionarioId(){
        val lancamento: Page<Lancamento>? = lancamentoService?.buscarPorFuncioanrioId(id, PageRequest.of(1,10))
        Assertions.assertNotNull(lancamento)
    }

    @Test
    fun testBuscarLancamentoPorId(){
//        val lancamento: Lancamento? = lancamentoService?.buscarPorId(id)
//        Assertions.assertNotNull(lancamento)
    }

    @Test
    fun testPersistirLancamento(){
        val lancamento: Lancamento? = lancamentoService?.persistir(lancamento())
        Assertions.assertNotNull(lancamento)
    }

    private fun lancamento(): Lancamento = Lancamento(Date(),TipoEnum.INICIO_TRABALHO, id)
}